//
//  Owner.h
//
//  Created by Thiago Santos on 31/05/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Owner : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *avatarUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
