//
//  Items.h
//
//  Created by Thiago Santos on 31/05/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Owner;

@interface Items : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *itemsDescription;
@property (nonatomic, assign) double openedCount;
@property (nonatomic, assign) double forksCount;
@property (nonatomic, assign) double stargazersCount;
@property (nonatomic, strong) Owner *owner;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
