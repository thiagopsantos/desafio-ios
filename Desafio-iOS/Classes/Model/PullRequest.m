//
//  BaseClass.m
//
//  Created by Thiago Santos on 31/05/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "PullRequest.h"
#import "User.h"

NSString *const kBaseClassTitle = @"title";
NSString *const kBaseClassBody = @"body";
NSString *const kBaseClassUser = @"user";
NSString *const kBaseClassCreatedAt = @"created_at";
NSString *const kBaseClassHtmlUrl = @"html_url";

@interface PullRequest ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PullRequest

@synthesize title = _title;
@synthesize body = _body;
@synthesize user = _user;
@synthesize createdAt = _createdAt;
@synthesize htmlUrl = _htmlUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kBaseClassTitle fromDictionary:dict];
            self.body = [self objectOrNilForKey:kBaseClassBody fromDictionary:dict];
            self.user = [User modelObjectWithDictionary:[dict objectForKey:kBaseClassUser]];
            self.createdAt = [self objectOrNilForKey:kBaseClassCreatedAt fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kBaseClassHtmlUrl fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];

    [mutableDict setValue:self.title forKey:kBaseClassTitle];
    [mutableDict setValue:self.body forKey:kBaseClassBody];
    [mutableDict setValue:self.createdAt forKey:kBaseClassCreatedAt];
    [mutableDict setValue:self.htmlUrl forKey:kBaseClassHtmlUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kBaseClassTitle];
    self.body = [aDecoder decodeObjectForKey:kBaseClassBody];
    self.user = [aDecoder decodeObjectForKey:kBaseClassUser];
    self.createdAt = [aDecoder decodeObjectForKey:kBaseClassCreatedAt];
    self.htmlUrl = [aDecoder decodeObjectForKey:kBaseClassHtmlUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_title forKey:kBaseClassTitle];
    [aCoder encodeObject:_body forKey:kBaseClassBody];
    [aCoder encodeObject:_user forKey:kBaseClassUser];
    [aCoder encodeObject:_createdAt forKey:kBaseClassCreatedAt];
    [aCoder encodeObject:_htmlUrl forKey:kBaseClassHtmlUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    PullRequest *copy = [[PullRequest alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.body = [self.body copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
