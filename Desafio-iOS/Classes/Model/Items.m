//
//  Items.m
//
//  Created by Thiago Santos on 31/05/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Items.h"
#import "Owner.h"

NSString *const kItemsName = @"name";
NSString *const kItemsForksCount = @"forks_count";
NSString *const kItemsStargazersCount = @"stargazers_count";
NSString *const kItemsDescription = @"description";
NSString *const kItemsOwner = @"owner";
NSString *const kItemsOpenIssuesCount = @"open_issues_count";

@interface Items ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Items

@synthesize owner = _owner;
@synthesize itemsDescription = _itemsDescription;
@synthesize name = _name;
@synthesize forksCount = _forksCount;
@synthesize stargazersCount = _stargazersCount;
@synthesize openedCount = _openedCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        
        self.owner = [Owner modelObjectWithDictionary:[dict objectForKey:kItemsOwner]];
        self.itemsDescription = [self objectOrNilForKey:kItemsDescription fromDictionary:dict];
        self.name = [self objectOrNilForKey:kItemsName fromDictionary:dict];
        self.forksCount = [[self objectOrNilForKey:kItemsForksCount fromDictionary:dict] doubleValue];
        self.stargazersCount = [[self objectOrNilForKey:kItemsStargazersCount fromDictionary:dict] doubleValue];
        self.openedCount = [[self objectOrNilForKey:kItemsOpenIssuesCount fromDictionary:dict] doubleValue];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kItemsOwner];
    [mutableDict setValue:self.itemsDescription forKey:kItemsDescription];
    [mutableDict setValue:self.name forKey:kItemsName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forksCount] forKey:kItemsForksCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.stargazersCount] forKey:kItemsStargazersCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openedCount] forKey:kItemsOpenIssuesCount];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.owner = [aDecoder decodeObjectForKey:kItemsOwner];
    self.itemsDescription = [aDecoder decodeObjectForKey:kItemsDescription];
    self.name = [aDecoder decodeObjectForKey:kItemsName];
    self.forksCount = [aDecoder decodeDoubleForKey:kItemsForksCount];
    self.stargazersCount = [aDecoder decodeDoubleForKey:kItemsStargazersCount];
    self.openedCount = [aDecoder decodeDoubleForKey:kItemsOpenIssuesCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_owner forKey:kItemsOwner];
    [aCoder encodeObject:_itemsDescription forKey:kItemsDescription];
    [aCoder encodeObject:_name forKey:kItemsName];
    [aCoder encodeDouble:_forksCount forKey:kItemsForksCount];
    [aCoder encodeDouble:_stargazersCount forKey:kItemsStargazersCount];
    [aCoder encodeDouble:_openedCount forKey:kItemsOpenIssuesCount];
}

- (id)copyWithZone:(NSZone *)zone
{
    Items *copy = [[Items alloc] init];
    
    if (copy) {
        
        copy.owner = [self.owner copyWithZone:zone];
        copy.itemsDescription = [self.itemsDescription copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.forksCount = self.forksCount;
        copy.stargazersCount = self.stargazersCount;
        copy.openedCount = self.openedCount;
    }
    
    return copy;
}


@end
