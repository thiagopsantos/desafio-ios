//
//  Owner.m
//
//  Created by Thiago Santos on 31/05/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Owner.h"

NSString *const kOwnerLogin = @"login";
NSString *const kOwnerAvatarUrl = @"avatar_url";

@interface Owner ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Owner

@synthesize login = _login;
@synthesize avatarUrl = _avatarUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.login = [self objectOrNilForKey:kOwnerLogin fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kOwnerAvatarUrl fromDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.login forKey:kOwnerLogin];
    [mutableDict setValue:self.avatarUrl forKey:kOwnerAvatarUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.login = [aDecoder decodeObjectForKey:kOwnerLogin];
    self.avatarUrl = [aDecoder decodeObjectForKey:kOwnerAvatarUrl];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_login forKey:kOwnerLogin];
    [aCoder encodeObject:_avatarUrl forKey:kOwnerAvatarUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    Owner *copy = [[Owner alloc] init];
    
    if (copy) {

        copy.login = [self.login copyWithZone:zone];
        copy.avatarUrl = [self.avatarUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
