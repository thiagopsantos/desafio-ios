//
//  Connection.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DownloadCallback)(BOOL success, NSInteger code, id result);

@interface Connection : NSObject

+ (void)downloadRepositoryWithName:(NSString *)name
                           andPage:(NSUInteger)page
                          callback:(DownloadCallback)callback;

+ (void)downloadRepositoryDetailWithUser:(NSString *)user
                           andRepository:(NSString *)repository
                                callback:(DownloadCallback)callback;

@end
