//
//  Connection.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "Connection.h"
#import "AFNetworking.h"

@implementation Connection

+ (void)downloadRepositoryWithName:(NSString *)name
                           andPage:(NSUInteger)page
                          callback:(DownloadCallback)callback{
    
    NSString *url = [NSString stringWithFormat:URL_REPOSITORY, name, (unsigned long)page];
    NSLog(@"url: %@", url);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        NSArray *items = [responseObject valueForKey:@"items"];
        callback(YES, httpResponse.statusCode, items);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        callback(NO, httpResponse.statusCode, nil);
    }];
}

+ (void)downloadRepositoryDetailWithUser:(NSString *)user
                           andRepository:(NSString *)repository
                                callback:(DownloadCallback)callback{

    NSString *url = [NSString stringWithFormat:URL_REPOSITORY_DETAIL, user, repository];
    NSLog(@"url: %@", url);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        callback(YES, httpResponse.statusCode, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        callback(NO, httpResponse.statusCode, nil);
    }];
}

@end
