//
//  Constants.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define REPOSITORY_NAME @"Java"

#define URL_REPOSITORY @"https://api.github.com/search/repositories?q=language:%@&sort=stars&page=%lu&per_page=20"
#define URL_REPOSITORY_DETAIL @"https://api.github.com/repos/%@/%@/pulls"

#define screenWidth [[UIScreen mainScreen] bounds].size.width
#define screenHeight [[UIScreen mainScreen] bounds].size.height

#endif /* Constants_h */
