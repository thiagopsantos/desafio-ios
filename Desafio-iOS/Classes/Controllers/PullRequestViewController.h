//
//  PullRequestViewController.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Items.h"
#import "Owner.h"

@interface PullRequestViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *tableview;
@property (nonatomic, weak) IBOutlet UILabel *lblOpened;
@property (nonatomic, strong) NSArray *pullRequests;
@property (nonatomic, strong) Items *items;

@end
