//
//  PullRequestViewController.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "PullRequestViewController.h"
#import "WebViewController.h"
#import "Connection.h"
#import "PullRequestCell.h"

@interface PullRequestViewController ()

@end

@implementation PullRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = self.items.name;
    self.lblOpened.text = [NSString stringWithFormat:@"%i oppened", (int)self.items.openedCount];
    
    [self fetchPullRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchPullRequest{
    
    [Connection downloadRepositoryDetailWithUser:self.items.owner.login
                                   andRepository:self.items.name
                                        callback:^(BOOL success, NSInteger code, id result) {
                                            
                                            if (result) {
                                                self.pullRequests = result;
                                                [self.tableview reloadData];
                                            }
                                        }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showWebview"]) {
        NSIndexPath *index = [self.tableview indexPathForSelectedRow];
        PullRequest *pullReq = [PullRequest modelObjectWithDictionary:[self.pullRequests objectAtIndex:index.row]];
        WebViewController *webViewC = (WebViewController *)segue.destinationViewController;
        webViewC.url = pullReq.htmlUrl;
        webViewC.title = pullReq.user.login;
    }
}

#pragma mark - Methods - UITableView DataSource/Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.pullRequests.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PullRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell updateCellWithPullRequest:[PullRequest modelObjectWithDictionary:[self.pullRequests objectAtIndex:indexPath.row]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"showWebview" sender:self];
}

@end
