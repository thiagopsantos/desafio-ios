//
//  RepositoryViewController.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 30/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "RepositoryViewController.h"
#import "PullRequestViewController.h"
#import "Connection.h"
#import "RepositoryCell.h"

@interface RepositoryViewController ()

@end

@implementation RepositoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = [NSString stringWithFormat:@"Github - %@", REPOSITORY_NAME];
    
    self.page = 1;
    self.items = [[NSMutableArray alloc]init];

    [self fetchRepository];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchRepository{
    
    [Connection downloadRepositoryWithName:REPOSITORY_NAME
                                   andPage:self.page
                                  callback:^(BOOL success, NSInteger code, id result) {
        
        if (result) {
            [self.items addObjectsFromArray:result];
            [self.tableview reloadData];
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *index = [self.tableview indexPathForSelectedRow];
        PullRequestViewController *pullRequestVC = (PullRequestViewController *)segue.destinationViewController;
        pullRequestVC.items = [Items modelObjectWithDictionary:[self.items objectAtIndex:index.row]];
    }
}

#pragma mark - Methods - UITableView DataSource/Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 95;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RepositoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row == self.items.count-1) {
        //show
        
        UIView *loadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 40)];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner setCenter:CGPointMake(loadView.frame.size.width/2.0, loadView.frame.size.height/2.0)];
        [loadView addSubview:spinner];
        [spinner startAnimating];
        self.tableview.tableFooterView = loadView;
        
        self.page = self.page+1;
        [self fetchRepository];
    }else{
        //hidden
        self.tableview.tableFooterView = nil;
    }
    
    [cell updateCellWithItems:[Items modelObjectWithDictionary:[self.items objectAtIndex:indexPath.row]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"showDetail" sender:self];
}


@end
