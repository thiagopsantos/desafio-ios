//
//  RepositoryViewController.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 30/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepositoryViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic) NSUInteger page;

@end
