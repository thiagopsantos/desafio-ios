//
//  WebViewController.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIWebView *webview;
@property (nonatomic, strong) NSString *url;

@end
