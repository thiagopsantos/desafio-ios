//
//  BaseViewController.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "BaseViewController.h"

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavColor:[UIColor blackColor]];
    [self setBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNavColor:(UIColor *)color{
    
    [self.navigationController.navigationBar setBarTintColor:color];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)setBackButton{
    
    UIBarButtonItem *myBackButton = [[UIBarButtonItem alloc]initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = myBackButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

@end
