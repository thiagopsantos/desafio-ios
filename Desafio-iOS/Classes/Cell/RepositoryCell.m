//
//  RepositoryCell.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "RepositoryCell.h"

@implementation RepositoryCell

- (void)updateCellWithItems:(Items *)items{
    
    NSString *urlImg = items.owner.avatarUrl;
    if (urlImg){
        [self.imgUser sd_setImageWithURL:[NSURL URLWithString:urlImg] placeholderImage:nil completed:nil];
    }
    
    self.lblUser.text = items.owner.login;
    self.lblName.text = items.name;
    self.lblDescription.text = items.itemsDescription;
    self.lblFork.text = [NSString stringWithFormat:@"%i", (int)items.forksCount];
    self.lblStars.text = [NSString stringWithFormat:@"%i", (int)items.stargazersCount];
}

@end
