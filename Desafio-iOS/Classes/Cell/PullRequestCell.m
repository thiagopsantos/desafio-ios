//
//  PullRequestCell.m
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import "PullRequestCell.h"

@implementation PullRequestCell

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.lblName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblName.frame);
    self.lblDescription.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription.frame);
}

- (void)updateCellWithPullRequest:(PullRequest *)pullRequest{
    
    NSString *urlImg = pullRequest.user.avatarUrl;
    if (urlImg){
        [self.imgUser sd_setImageWithURL:[NSURL URLWithString:urlImg] placeholderImage:nil completed:nil];
    }
    
    self.lblUser.text = pullRequest.user.login;
    self.lblName.text = pullRequest.title;
    self.lblDescription.text = pullRequest.body;
    self.lblDate.text = pullRequest.createdAt;
}

@end
