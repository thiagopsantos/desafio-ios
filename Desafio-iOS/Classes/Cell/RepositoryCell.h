//
//  RepositoryCell.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Items.h"
#import "Owner.h"

@interface RepositoryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgUser;
@property (nonatomic, weak) IBOutlet UILabel *lblUser;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblDescription;
@property (nonatomic, weak) IBOutlet UILabel *lblFork;
@property (nonatomic, weak) IBOutlet UILabel *lblStars;

- (void)updateCellWithItems:(Items *)items;

@end
