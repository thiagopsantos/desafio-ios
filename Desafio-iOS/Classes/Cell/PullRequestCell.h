//
//  PullRequestCell.h
//  Desafio-iOS
//
//  Created by Thiago Santos on 31/05/16.
//  Copyright © 2016 Thiago Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"
#import "User.h"

@interface PullRequestCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgUser;
@property (nonatomic, weak) IBOutlet UILabel *lblUser;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblDescription;

- (void)updateCellWithPullRequest:(PullRequest *)pullRequest;

@end
